var Spawner = {

    Run: function (Game) {
        var _maxHarvesters = 3;
        var _maxUpgraders = 4;
        var _maxBuilders = 4;
        var _availableEnergy = Game.spawns['Spawn1'].room.energyAvailable;

        var _sources = Game.spawns['Spawn1'].room.find(FIND_SOURCES);
        
        /* The below will move swap assigned energy sources between creeps */
        var _chosenSource = Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation;
        //If the chosen source is not a number, randomly choose it
        if (_chosenSource == NaN){
            Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = Math.floor((Math.random() * _sources.length));
            _chosenSource = Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation;
        } //Else if it is greater than the length of the sources, floor it back to 0. 
        else if (_chosenSource > _sources.length - 1){
            Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = 0;
            _chosenSource = Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation;
        } 
        else if (_chosenSource < _sources.length){
            //Leaving this blank, as this is more of a check to see if valid
        }
        else { //Something goes wrong if it gets here, so I just reset it
            Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = Math.floor((Math.random() * _sources.length));
            chosenSource = Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation;
        }


        var _hostiles = Game.spawns['Spawn1'].room.find(FIND_HOSTILE_CREEPS);

        if(_hostiles.length > 0){
            return;
        }

        //console.log(Game.spawns['Spawn1'].room.energyAvailable)

        /*
            Build priorities:
                Harvesters
                Upgraders
                Builders
        */


        //Loops creep names and deletes dead ones
        for (var _name in Memory.creeps) {
            if (!Game.creeps[_name]) {
                delete Memory.creeps[_name];
                //console.log('Clearing non-existing creep memory:', _name);
            }
        }

        var _creeps = _.filter(Game.creeps, (creep) => creep.memory.role == 'Harvester');
        if (_creeps.length < _maxHarvesters) { // Spawn a harvester
            var newName = 'Harvester' + Game.time;
            //console.log('Spawning new harvester: ' + newName);
            if (_availableEnergy < 400) {
                Game.spawns['Spawn1'].spawnCreep([WORK, CARRY, MOVE], newName,
                    { memory: { role: 'Harvester', m_assignedSource: _chosenSource } });
                Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = _chosenSource + 1;
            }
            else {
                Game.spawns['Spawn1'].spawnCreep([WORK, WORK, CARRY, CARRY, MOVE], newName,
                    { memory: { role: 'Harvester', m_assignedSource: _chosenSource } });
                Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = _chosenSource + 1;
            }
        }
        else { 
            _creeps = _.filter(Game.creeps, (creep) => creep.memory.role == 'Upgrader');
            if (_creeps.length < _maxUpgraders) { // Spawn an upgrader
                var newName = 'Upgrader' + Game.time;
                //console.log('Spawning new Upgrader: ' + newName);
                if (_availableEnergy < 400) {
                    Game.spawns['Spawn1'].spawnCreep([WORK, CARRY, MOVE], newName,
                        { memory: { role: 'Upgrader', m_assignedSource: _chosenSource } });
                    Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = _chosenSource + 1;

                }
                else {
                    Game.spawns['Spawn1'].spawnCreep([WORK, WORK, CARRY, CARRY, MOVE], newName,
                        { memory: { role: 'Upgrader', m_assignedSource: _chosenSource } });
                    Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = _chosenSource + 1;

                }
            }
            else {
                _creeps = _.filter(Game.creeps, (creep) => creep.memory.role == 'Builder');
                if (_creeps.length < _maxBuilders) { // Spawn a builder
                    var newName = 'Builder' + Game.time;
                    //console.log('Spawning new Builder: ' + newName);
                    if (_availableEnergy < 400) {
                        Game.spawns['Spawn1'].spawnCreep([WORK, CARRY, MOVE], newName,
                            { memory: { role: 'Builder', m_assignedSource: _chosenSource } });
                        Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = _chosenSource + 1;

                    }
                    else {
                        Game.spawns['Spawn1'].spawnCreep([WORK, WORK, CARRY, CARRY, MOVE], newName,
                            { memory: { role: 'Builder', m_assignedSource: _chosenSource } });
                        Game.spawns['Spawn1'].room.memory.m_previousSpawnedSourceAllocation = _chosenSource + 1;

                    }
                }

            }
        }

        if (Game.spawns['Spawn1'].spawning) {
            var _spawningCreep = Game.creeps[Game.spawns['Spawn1'].spawning.name];
            Game.spawns['Spawn1'].room.visual.text(
                '🛠️' + _spawningCreep.memory.role,
                Game.spawns['Spawn1'].pos.x + 1,
                Game.spawns['Spawn1'].pos.y,
                { align: 'left', opacity: 0.8 });
        }
    }
};

module.exports = Spawner;

