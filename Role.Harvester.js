var RoleHarvester = {

    /** @param {Creep} creep **/
    Run: function(creep) {
        var _remainingCapacity = creep.store.getFreeCapacity();
        var _storageLocations = creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_CONTAINER ||
                        structure.structureType == STRUCTURE_EXTENSION ||
                        structure.structureType == STRUCTURE_SPAWN ||
                        structure.structureType == STRUCTURE_TOWER) && 
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
            }});
        if(_storageLocations.length < 0)
        {
            creep.memory.m_harvesting = false;
            creep.moveTo(4,39);
        }
	    else if(_remainingCapacity > 0) {
            var _sources = creep.room.find(FIND_SOURCES);
            creep.memory.m_harvesting = true;
            if(creep.harvest(_sources[creep.memory.m_assignedSource]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(_sources[creep.memory.m_assignedSource], {visualizePathStyle: {stroke: '#ffaa00'}});
            }
        }
        else {
            creep.memory.m_harvesting = false;
            if(_storageLocations.length > 0) {
                if(creep.transfer(_storageLocations[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(_storageLocations[0], {visualizePathStyle: {stroke: '#ffffff'}});
                }
            }
        }
	}
};

module.exports = RoleHarvester;