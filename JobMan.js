var JobMan = {

    g_harvestJobs     : [],
    g_repairJobs      : [],
    g_wallRepairJobs  : [],
    g_buildJobs       : [],
    g_upgradeJobs     : [],
    g_creeps          : [],
    g_ownedRooms      : [],
    g_harvesterCount  : 0,
    g_repairerCount   : 0,
    g_builderCount    : 0,
    g_upgraderCount   : 0, 
    g_creepCount      : 0,


    /*
    This class will do the following: 
        Get a list of all required jobs
        Assign a job to a worker when requested
        Prioratise jobs to what is required dynamically

        The goal of this is to remove the wasted resources spent on workers (harvesters, builders, upgraders etc)
        This will be set on a global scale to allow resource/bot allocation between rooms. 

        Priority Logic:
            Number of harvesters - should be percentage based depeding on minimum amount of energy (e.g. should assign more than necessary per room when under 300, spawn minimum). 
                over 90%: 5%
                Over 75%: 10%
                Over 50%: 20%
                Over 25% storage: 25%
                Under 10%: 50%
                Under 300 energy: Minimum 2


            Repairers - This should be priopratised over building, but only ever selected when there are things to repair.  
            Builders - Should only be selected when there are things to build.
            Minimal level of upgraders == 1 per room. This will prevent the room controller from being downgraded, while allowing at least a small level of growth. 
                Otherwise, should only be assigned when there are left over bots

    */


    Run: function (Game) {
        this.PopulateJobs(Game);
    },

    PopulateJobs: function(Game) {
        //Get's a list of all upgrade jobs needed
        //console.log(Game.rooms[0].controller.my);
        this.g_ownedRooms       = [];
        this.g_harvestJobs      = [];
        this.g_repairJobs       = [];
        this.g_buildJobs        = [];
        this.g_upgradeJobs      = [];
        this.g_creeps           = [];
        this.g_ownedRooms       = [];
        this.g_wallRepairJobs   = [];
        
        var _key = 0;
        for (_key in Game.rooms){
            if(Game.rooms[_key].controller.my){
                //console.log(_key);
                this.g_ownedRooms.push(Game.rooms[_key]);
                //console.log(Game.rooms[_key]);
            }
        }

        for( var i = 0; i < this.g_ownedRooms.length ; i++ ){
            var _room = this.g_ownedRooms[i];

            console.log(_room);
            //Gets the repair jobs that are not walls
            this.g_repairJobs.push(_room.find(FIND_STRUCTURES, {
				filter: (_structure) => {
                    return ((_structure.hits < (_structure.hitsMax)> 0) &&
                    _structure.structureType != STRUCTURE_WALL);
				}}));

            console.log("repair jobs = " + this.g_repairJobs.length);

            this.g_wallRepairJobs.push(_room.find(FIND_STRUCTURES, {
				filter: (_structure) => {
                    return ((_structure.hits < (_structure.hitsMax * 0.0005)> 0) &&
                    _structure.structureType === STRUCTURE_WALL);
				}}));
            console.log("wall repair jobs = " + this.g_wallRepairJobs.length);
            
            this.g_buildJobs.push(_room.find(FIND_CONSTRUCTION_SITES));
            console.log("Buildjobs = " + this.g_buildJobs.length);

            this.g_upgradeJobs.push(_room.controller);
            console.log("upgradeJobs = " + this.g_upgradeJobs.length);
        }
    }


};

module.exports = JobMan;

