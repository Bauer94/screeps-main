var g_roleHarvester = require('Role.Harvester');
var g_roleUpgrader = require('Role.Upgrader');
var g_roleBuilder = require('Role.Builder');
var g_roleWorker = require('Role.Worker');
var g_spawner = require('Spawner')
var g_jobMan = require('JobMan');


/*TODO
*Look into auto spawning of aged creeps - StructureSpawn.renewCreep
*Spawn defences (towers walls and creeps): Game.spawns['Spawn1'].room.createConstructionSite( 23, 22, STRUCTURE_TOWER );
*
*
*/

module.exports.loop = function () {
    console.log("---------------------------------------------"); // Adds a seperater between log frames
    console.log(Game.time);
    
    g_jobMan.Run(Game);

    for (var _name in Game.creeps) {
        var _creep = Game.creeps[_name];

        switch (_creep.memory.role) {
            case 'Harvester':
                g_roleHarvester.Run(_creep);
                break;
            case 'Builder':
                g_roleBuilder.Run(_creep);
                break;
            case 'Upgrader':
                g_roleUpgrader.Run(_creep);
                break;
            case `Worker`:
                g_roleWorker.Run(_creep);
                break;
            default:
                break;
        }
    }
    
    g_spawner.Run(Game);
    //console.log(Game.energyAvailable)
}


/*var tower = Game.getObjectById('5f60b9d2793c050e2a15c366');
if(tower) {
    var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
        filter: (structure) => structure.hits < structure.hitsMax
    });
    if(closestDamagedStructure) {
        tower.repair(closestDamagedStructure);
    }

    var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
    if(closestHostile) {
        tower.attack(closestHostile);
    }
}*/