var RoleUpgrader = {

    /** @param {Creep} creep **/
    Run: function(creep){

        var _remainingCapacity = creep.store.getFreeCapacity();
        var _usedCapacity = creep.store.getUsedCapacity();
	    if(_remainingCapacity > 0 && !creep.memory.m_upgrading) {
            var _sources = creep.room.find(FIND_SOURCES);
            creep.memory.m_harvesting = true;
            if(creep.harvest(_sources[creep.memory.m_assignedSource]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(_sources[creep.memory.m_assignedSource], {visualizePathStyle: {stroke: '#ffaa00'}});
            }
        }
        else {
            creep.memory.m_upgrading = true;
            creep.memory.m_harvesting = false;
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            }
            if (_usedCapacity == 0)
            {
                creep.memory.m_upgrading = false;
                creep.say('🔄 Harvest');
            }
        }
	}
}; 

module.exports = RoleUpgrader;