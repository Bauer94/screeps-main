var RoleBuilder = {

	/** @param {Creep} creep **/
	Run: function (creep) {
		var _remainingCapacity = creep.store.getFreeCapacity();

		if (creep.memory.building && creep.store[RESOURCE_ENERGY] == 0) {
			creep.memory.building = false;
			creep.say('🔄 harvest');
		}
		if (!creep.memory.building && creep.store.getFreeCapacity() == 0) {
			creep.memory.building = true;
			creep.say('🚧 build');
		}

		if (creep.memory.building) {
			//Adds structures that are damaged or below full health to be repaired
			var _repairTargets = creep.room.find(FIND_STRUCTURES, {
				filter: (_structure) => {
					return (_structure.hits < (_structure.hitsMax * 0.0005)> 0 );
				}});
			//Adds structures yet to be complete
			var _BuildTargets = creep.room.find(FIND_CONSTRUCTION_SITES);
			if(_repairTargets.length){
				console.log("struct type = " + creep.repair(_repairTargets[0].structureType));
				if (creep.repair(_repairTargets[0]) == ERR_NOT_IN_RANGE) {
					creep.moveTo(_repairTargets[0], { visualizePathStyle: { stroke: '#ffffff' } });
				}
			}
			else if (_BuildTargets.length) {
				if (creep.build(_BuildTargets[0]) == ERR_NOT_IN_RANGE) {
					creep.moveTo(_BuildTargets[0], { visualizePathStyle: { stroke: '#ffffff' } });
				}
			}
			else{
				var _repairTargets = creep.room.find(FIND_STRUCTURES, {
					filter: (_structure) => {
						return (_structure.hits < (_structure.hitsMax)) > 0;
					}});
					
				creep.memory.building = false;
			}
		}
		else {
			if (_remainingCapacity > 0) {
				var _sources = creep.room.find(FIND_SOURCES);
				creep.memory.m_harvesting = true;
				if (creep.harvest(_sources[creep.memory.m_assignedSource]) == ERR_NOT_IN_RANGE) {
					creep.moveTo(_sources[creep.memory.m_assignedSource], { visualizePathStyle: { stroke: '#ffaa00' } });
				}
			}
			else {
				creep.memory.m_harvesting = false;
				var _storageLocations = creep.room.find(FIND_STRUCTURES, {
					filter: (structure) => {
						return (structure.structureType == STRUCTURE_EXTENSION ||
							structure.structureType == STRUCTURE_CONTAINER ||
							structure.structureType == STRUCTURE_SPAWN ||
							structure.structureType == STRUCTURE_TOWER) &&
							structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
					}
				});
				if (_storageLocations.length > 0) {
					if (creep.transfer(_storageLocations[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
						creep.moveTo(_storageLocations[0], { visualizePathStyle: { stroke: '#ffffff' } });
					}
				}
			}
		}


		//var _usedCapacity = creep.store.getUsedCapacity();

	}
};

module.exports = RoleBuilder;